#if UNITY_EDITOR
using System.Reflection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using Wazzapps.Localization;

namespace Wazzapps.Screenshoter
{
    public class ScreenshotObject : MonoBehaviour
    {
        private Dictionary<Camera, CameraSetup> cameraSetups = new Dictionary<Camera, CameraSetup>();
        private Dictionary<Canvas, bool> canvasesStates = new Dictionary<Canvas, bool>();
        private float timeScale;
        private Material skyBoxMaterial;
        private ScreenshotTakerSettings settings;

        private class CameraSetup
        {
            public LayerMask TargetLayers;
            public Color ClearColor;
            public CameraClearFlags ClearFlags;
        }

        public void Take(ScreenshotTakerSettings settings)
        {
            var folderName = settings.ScreenshotsFolder;
            if (!Directory.Exists(folderName)) Directory.CreateDirectory(folderName);

            timeScale = Time.timeScale;
            this.settings = settings;
            StartCoroutine(TakeScreenshot());
        }

        private void CacheCameras()
        {
            cameraSetups.Clear();
            var cameras = FindObjectsOfType<Camera>();
            foreach (var camera in cameras)
            {
                cameraSetups.Add(camera, new CameraSetup()
                {
                    TargetLayers = camera.cullingMask, ClearColor = camera.backgroundColor,
                    ClearFlags = camera.clearFlags
                });
            }

            canvasesStates.Clear();
            var canvases = FindObjectsOfType<Canvas>();
            foreach (var canvas in canvases)
            {
                canvasesStates.Add(canvas, canvas.enabled);
            }

            skyBoxMaterial = RenderSettings.skybox;
        }

        private void ResetCamera()
        {
            RenderSettings.skybox = skyBoxMaterial;

            foreach (var cam in cameraSetups)
            {
                cam.Key.cullingMask = cam.Value.TargetLayers;
                cam.Key.backgroundColor = cam.Value.ClearColor;
                cam.Key.clearFlags = cam.Value.ClearFlags;
            }

            foreach (var canvas in canvasesStates)
            {
                canvas.Key.enabled = canvas.Value;
            }
        }

        private void SetGameplayCamera()
        {
            ResetCamera();

            foreach (var canvas in canvasesStates)
            {
                if (settings.ForceCanvasesToGameplay.Contains(canvas.Key.name)) continue;
                canvas.Key.enabled = false;
            }
        }

        private void SetUiCamera(Color clearColor)
        {
            RenderSettings.skybox = null;

            foreach (var cam in cameraSetups)
            {
                cam.Key.cullingMask = LayerMask.GetMask("UI");
                cam.Key.clearFlags = cam.Value.ClearFlags;
                cam.Key.backgroundColor = clearColor;
            }

            foreach (var canvas in canvasesStates)
            {
                if (settings.ForceCanvasesToGameplay.Contains(canvas.Key.name))
                {
                    canvas.Key.enabled = false;
                }
            }
        }

        private IEnumerator TakeScreenshot()
        {
            Vector2Int currentSize = new Vector2Int(Screen.width, Screen.height);
            SystemLanguage currentLanguage = GetLanguage();
            ToggleGizmos(false);
            CacheCameras();

            if (settings.MakeGameplayScreenshot)
            {
                SetGameplayCamera();
                Time.timeScale = 0;
                foreach (var size in settings.Sizes)
                {
                    if (!GameViewResizer.SetScreenSize(size.x, size.y))
                    {
                        Debug.LogWarning($"Unable to set screen size to {size.x}x{size.y}!");
                        continue;
                    }

                    yield return TakeSingleScreenshot(settings.ScreenshotsFolder, string.Format(
                        settings.ScreenshotsFileFormat, settings.ScreenshotNameIndex,
                        size.x, size.y,
                        GetShortLangCode(GetLanguage()), "gameplay"));
                }

                Time.timeScale = timeScale;
                ResetCamera();
            }

            if (settings.MakeCombinedScreenshot || settings.MakeUiScreenshot)
            {
                foreach (var language in settings.Languages)
                {
                    SetLanguage(language);
                    string langCode = GetShortLangCode(language);
                    yield return new WaitForSecondsRealtime(settings.DelayBetweenLangChange);

                    Time.timeScale = 0;
                    foreach (var size in settings.Sizes)
                    {
                        if (!GameViewResizer.SetScreenSize(size.x, size.y))
                        {
                            Debug.LogWarning($"Unable to set screen size to {size.x}x{size.y}!");
                            continue;
                        }

                        yield return null;

                        if (settings.MakeCombinedScreenshot)
                        {
                            ResetCamera();
                            yield return TakeSingleScreenshot(settings.ScreenshotsFolder, string.Format(
                                settings.ScreenshotsFileFormat, settings.ScreenshotNameIndex,
                                size.x, size.y,
                                langCode, "full"));
                        }

                        if (settings.MakeUiScreenshot)
                        {
                            yield return TakeUiScreenshot(settings.ScreenshotsFolder, string.Format(
                                settings.ScreenshotsFileFormat, settings.ScreenshotNameIndex,
                                size.x, size.y,
                                langCode, "ui"));
                        }
                    }

                    Time.timeScale = timeScale;
                }
            }

            Time.timeScale = timeScale;
            ResetCamera();
            ToggleGizmos(true);
            GameViewResizer.SetScreenSize(currentSize.x, currentSize.y);
            SetLanguage(currentLanguage);
            ScreenshotTakerSettingsEditor.OpenFolder(settings.ScreenshotsFolder);
            DestroyImmediate(gameObject);
        }

        private static void SetLanguage(SystemLanguage language)
        {
            FieldInfo fi = typeof(Localizator).GetField("ForceLangKey", BindingFlags.NonPublic | BindingFlags.Static);
            if (fi != null)
            {
                PlayerPrefs.SetInt(fi.GetValue(null).ToString(), (int) language);
            }

            else
            {
                PlayerPrefs.SetInt("localizatorForceLanguage", (int) language);
            }

            Localizator.OnLanguageChanged?.Invoke();
        }

        private static SystemLanguage GetLanguage()
        {
            FieldInfo fi = typeof(Localizator).GetField("ForceLangKey", BindingFlags.NonPublic | BindingFlags.Static);
            if (fi != null)
            {
                return (SystemLanguage) PlayerPrefs.GetInt(fi.GetValue(null).ToString(),
                    (int) Application.systemLanguage);
            }

            return Application.systemLanguage;
        }

        private static string GetShortLangCode(SystemLanguage language)
        {
            if (language == SystemLanguage.Russian) return "ru";
            if (language == SystemLanguage.English) return "en";
            if (language == SystemLanguage.Korean) return "kr";
            if (language == SystemLanguage.Japanese) return "jp";
            return language.ToString();
        }

        private IEnumerator TakeUiScreenshot(string folderName, string filename)
        {
            SetUiCamera(Color.black);

            yield return new WaitForEndOfFrame();
            Texture2D bScreenShot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            bScreenShot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            bScreenShot.Apply();

            SetUiCamera(Color.white);

            yield return new WaitForEndOfFrame();
            Texture2D wScreenShot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            wScreenShot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            wScreenShot.Apply();

            var screenShot = new Texture2D(Screen.width, Screen.height, TextureFormat.ARGB32, false);
            for (int y = 0; y < screenShot.height; ++y)
            {
                for (int x = 0; x < screenShot.width; ++x)
                {
                    float alpha = wScreenShot.GetPixel(x, y).r - bScreenShot.GetPixel(x, y).r;
                    alpha = 1.0f - alpha;
                    Color color;
                    if (alpha <= 0.001f)
                    {
                        color = Color.clear;
                    }
                    else
                    {
                        color = bScreenShot.GetPixel(x, y) / alpha;
                    }

                    color.a = alpha;
                    screenShot.SetPixel(x, y, color);
                }
            }

            byte[] bytes = screenShot.EncodeToPNG();
            SaveToFile($"{folderName}/{filename}", bytes);
        }

        private IEnumerator TakeVideoFrame(List<byte[]> frames)
        {
            yield return new WaitForEndOfFrame();
            Texture2D screenShot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            screenShot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            screenShot.Apply();

            byte[] bytes = screenShot.EncodeToPNG();
            frames.Add(bytes);
        }

        private IEnumerator TakeSingleScreenshot(string folderName, string filename)
        {
            yield return new WaitForEndOfFrame();
            Texture2D screenShot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            screenShot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            screenShot.Apply();

            byte[] bytes = screenShot.EncodeToPNG();
            SaveToFile($"{folderName}/{filename}", bytes);
        }

        private void SaveToFile(string filename, byte[] bytes)
        {
            if (File.Exists(filename))
            {
                Debug.LogError($"File {filename} was replaced!");
                File.Delete($"{filename}");
            }

            File.WriteAllBytes($"{filename}", bytes);
        }

        public static void ToggleGizmos(bool gizmosOn)
        {
            int val = gizmosOn ? 1 : 0;
            Assembly asm = Assembly.GetAssembly(typeof(Editor));
            Type type = asm.GetType("UnityEditor.AnnotationUtility");
            if (type != null)
            {
                MethodInfo getAnnotations =
                    type.GetMethod("GetAnnotations", BindingFlags.Static | BindingFlags.NonPublic);
                MethodInfo setGizmoEnabled =
                    type.GetMethod("SetGizmoEnabled", BindingFlags.Static | BindingFlags.NonPublic);
                MethodInfo setIconEnabled =
                    type.GetMethod("SetIconEnabled", BindingFlags.Static | BindingFlags.NonPublic);
                var annotations = getAnnotations.Invoke(null, null);
                foreach (object annotation in (IEnumerable) annotations)
                {
                    Type annotationType = annotation.GetType();
                    FieldInfo classIdField =
                        annotationType.GetField("classID", BindingFlags.Public | BindingFlags.Instance);
                    FieldInfo scriptClassField =
                        annotationType.GetField("scriptClass", BindingFlags.Public | BindingFlags.Instance);
                    if (classIdField != null && scriptClassField != null)
                    {
                        int classId = (int) classIdField.GetValue(annotation);
                        string scriptClass = (string) scriptClassField.GetValue(annotation);
#if UNITY_2019_1_OR_NEWER
                        setGizmoEnabled.Invoke(null, new object[] {classId, scriptClass, val, false});
#else
                        setGizmoEnabled.Invoke(null, new object[] {classId, scriptClass, val});
#endif
                        setIconEnabled.Invoke(null, new object[] {classId, scriptClass, val});
                    }
                }
            }
        }
    }
}
#endif