#if UNITY_EDITOR
using UnityEditor;
using System.Diagnostics;
#endif
using System.Collections.Generic;
using UnityEngine;

namespace Wazzapps.Screenshoter
{
    [CreateAssetMenu(menuName = nameof(ScreenshotTakerSettings), fileName = nameof(ScreenshotTakerSettings))]
    public class ScreenshotTakerSettings : ScriptableObject
    {
        public string ScreenshotsFolder = "Screenshots";

        public int ScreenshotNameIndex = 1;
        public string ScreenshotsFileFormat = "screen{0:000}_{1}x{2}_{3}_{4}.png";
        public float DelayBetweenLangChange = 0.2f;

        [Space] public Vector2Int[] Sizes =
        {
            new Vector2Int(1080, 1960), new Vector2Int(480, 800)
        };

        public SystemLanguage[] Languages =
        {
            SystemLanguage.English, SystemLanguage.Russian, SystemLanguage.Korean, SystemLanguage.Japanese
        };

        public bool MakeCombinedScreenshot = true;
        public bool MakeGameplayScreenshot = true;
        public bool MakeUiScreenshot = true;

        // Human evolution canvases
        [Space] public List<string> ForceCanvasesToGameplay = new List<string> {"BGCanvas", "SpaceContainer", "SpaceMap(Clone)"};

        [Space] public bool PlayModeFromFirstScene = true;
    }


#if UNITY_EDITOR
    [CustomEditor(typeof(ScreenshotTakerSettings))]
    public class ScreenshotTakerSettingsEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var settings = (ScreenshotTakerSettings) target;
            DrawDefaultInspector();
            GUILayout.Space(50);

            var color = GUI.backgroundColor;
            GUI.backgroundColor = new Color32(134, 212, 99, 255);
            if (GUILayout.Button("Take screenshot", GUILayout.Height(100)))
            {
                ScreenshotTaker.Capture((ScreenshotTakerSettings) target);
            }
            GUI.backgroundColor = color;

            GUILayout.Space(30);
            if (GUILayout.Button("Open screenshots folder", GUILayout.Height(30)))
            {
                OpenFolder(settings.ScreenshotsFolder);
            }
        }

        public static void OpenFolder(string folder)
        {
            var fullFolder = $"./{folder}";
#if UNITY_EDITOR_OSX
            Process process = new Process();
            process.StartInfo.FileName = "open";
            process.StartInfo.Arguments = fullFolder;
            process.StartInfo.UseShellExecute = false;
            process.Start();
#else
                fullFolder = fullFolder.Replace(@"/", @"\");
                Process.Start("explorer.exe", fullFolder);
#endif
        }
    }
#endif
}