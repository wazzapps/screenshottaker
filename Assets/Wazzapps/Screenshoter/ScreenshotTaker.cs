﻿#if UNITY_EDITOR
using System;
using UnityEngine;
using UnityEditor;

namespace Wazzapps.Screenshoter
{
    public static class ScreenshotTaker
    {
        [MenuItem("Window/Screenshot/Screenshots Settings &#s")]
        private static void OpenSettings()
        {
            var settings = Resources.Load<ScreenshotTakerSettings>(nameof(ScreenshotTakerSettings));
            if (settings == null)
            {
                settings = ScriptableObject.CreateInstance<ScreenshotTakerSettings>();
            }

            Selection.activeObject = settings;
        }
        
        [MenuItem("Window/Screenshot/Take Screenshot Now &#d")]
        private static void CaptureNow()
        {
            var settings = Resources.Load<ScreenshotTakerSettings>(nameof(ScreenshotTakerSettings));
            if (settings == null)
            {
                settings = ScriptableObject.CreateInstance<ScreenshotTakerSettings>();
            }

            Capture(settings);
        }

        public static void Capture(ScreenshotTakerSettings settings)
        {
            if (!Application.isPlaying)
            {
                throw new Exception("ScreenshotTaker works only in play mode!");
            }

            var so = new GameObject("ScreenshotObject").AddComponent<ScreenshotObject>();
            so.Take(settings);
        }
    }
}
#endif