﻿#if UNITY_EDITOR
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Wazzapps.Screenshoter
{
    public static class GameViewResizer
    {
        static object gameViewSizesInstance;
        static MethodInfo getGroup;
        private static int screenIndex = 16; // Because have 16 indexes in my list.
        private static int gameViewProfilesCount;

        static GameViewResizer()
        {
            var sizesType = typeof(Editor).Assembly.GetType("UnityEditor.GameViewSizes");
            var singleType = typeof(ScriptableSingleton<>).MakeGenericType(sizesType);
            var instanceProp = singleType.GetProperty("instance");
            getGroup = sizesType.GetMethod("GetGroup");
            gameViewSizesInstance = instanceProp.GetValue(null, null);
        }

        public static bool SetScreenSize(int width, int height)
        {
#if UNITY_ANDROID
            GameViewSizeGroupType group = GameViewSizeGroupType.Android;
#else
            GameViewSizeGroupType group = GameViewSizeGroupType.iOS;
#endif
            int sizeId = FindSize(group, $"{width}x{height}", width < height);
            if (sizeId == -1)
            {
                AddCustomSize(GameViewSizeType.FixedResolution, group, width, height, $"{width}x{height}");
                sizeId = FindSize(group, $"{width}x{height}", width < height);
                if (sizeId == -1)
                {
                    Debug.LogError($"Unable to resize game window to {width}x{height}!");
                    return false;
                }
            }
            SetSize(sizeId);
            return true;
        }

        public enum GameViewSizeType
        {
            AspectRatio,
            FixedResolution
        }

        private static void SetSize(int index)
        {
            var gvWndType = typeof(Editor).Assembly.GetType("UnityEditor.GameView");
            var gvWnd = EditorWindow.GetWindow(gvWndType);
            var SizeSelectionCallback = gvWndType.GetMethod("SizeSelectionCallback",
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            SizeSelectionCallback.Invoke(gvWnd, new object[] {index, null});
        }

        public static void AddCustomSize(GameViewSizeType viewSizeType, GameViewSizeGroupType sizeGroupType, int width,
            int height, string text)
        {
            var group = GetGroup(sizeGroupType);
            var addCustomSize = getGroup.ReturnType.GetMethod("AddCustomSize"); // or group.GetType().
            var gvsType = typeof(Editor).Assembly.GetType("UnityEditor.GameViewSize");
            
            Assembly assembly = Assembly.Load("UnityEditor.dll");
            var ctor = gvsType.GetConstructor(new Type[] {assembly.GetType("UnityEditor.GameViewSizeType"), typeof(int), typeof(int), typeof(string)});
            var newSize = ctor.Invoke(new object[] {(int) viewSizeType, width, height, text});
            addCustomSize.Invoke(group, new object[] {newSize});
        }

        static object GetGroup(GameViewSizeGroupType type)
        {
            return getGroup.Invoke(gameViewSizesInstance, new object[] {(int) type});
        }

        public static int FindSize(GameViewSizeGroupType sizeGroupType, string text, bool isPortrait)
        {
            var group = GetGroup(sizeGroupType);
            var getDisplayTexts = group.GetType().GetMethod("GetDisplayTexts");
            var displayTexts = getDisplayTexts.Invoke(group, null) as string[];
            for (int i = 0; i < displayTexts.Length; i++)
            {
                string display = displayTexts[i];
                var items = display.Split('(');
                display = items[items.Length - 1].Trim();
                if(display.ToLowerInvariant().Contains("portrait") && !isPortrait) continue;
                if(display.ToLowerInvariant().Contains("landscape") && isPortrait) continue;
                
                Regex rgx = new Regex("[^x0-9]");
                display = rgx.Replace(display, "");

                if (string.Equals(display.Trim(), text))
                    return i;
            }

            return -1;
        }
    }
}
#endif