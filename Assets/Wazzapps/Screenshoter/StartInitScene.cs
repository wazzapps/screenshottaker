#if UNITY_EDITOR
using UnityEngine.SceneManagement;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Wazzapps.Screenshoter
{
    [InitializeOnLoadAttribute]
    public static class StartInitScene
    {
        private static bool isClearDebug;

        static StartInitScene()
        {
            EditorApplication.playModeStateChanged -= PlayModeStateChangedHandler;
            EditorApplication.playModeStateChanged += PlayModeStateChangedHandler;
            SceneManager.activeSceneChanged -= ActiveSceneChangedHandler;
            SceneManager.activeSceneChanged += ActiveSceneChangedHandler;
        }

        private static void PlayModeStateChangedHandler(PlayModeStateChange playModeStateChange)
        {
            if (playModeStateChange == PlayModeStateChange.EnteredPlayMode)
            {
                CheckScene();
            }
        }

        private static void ActiveSceneChangedHandler(Scene arg0, Scene arg1)
        {
            if (isClearDebug)
            {
                isClearDebug = false;
                ClearLog();
            }
        }

        private static void CheckScene()
        {
            var settings = Resources.Load<ScreenshotTakerSettings>(nameof(ScreenshotTakerSettings));
            if (settings == null)
            {
                settings = ScriptableObject.CreateInstance<ScreenshotTakerSettings>();
            }
            if (settings != null && !settings.PlayModeFromFirstScene) return;
            
            if (SceneManager.GetActiveScene() != SceneManager.GetSceneByBuildIndex(0))
            {
                SceneManager.LoadScene(0);
                isClearDebug = true;
            }
        }

        public static void ClearLog()
        {
            var assembly = Assembly.GetAssembly(typeof(Editor));
            var type = assembly.GetType("UnityEditor.LogEntries");
            var method = type.GetMethod("Clear");
            method.Invoke(new object(), null);
        }
    }
}
#endif